#导入使用的相关模块
import requests
from lxml import etree
import csv
import time

class BossSpider(object):

    def __init__(self):
        self.url = 'https://www.zhipin.com/c101270100/?query=测试&page={}&ka=page-{}'

        # 构建url列表(10页)
        self.urls = [self.url.format(i,i)for i in range(10)]

        #构造请求头
        self.headers = {
            'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36',
            'cookie':'sid=sem_pz_bdpc_dasou_title; JSESSIONID=""; __g=sem_pz_bdpc_dasou_title; _uab_collina=155105932826456626422895; Hm_lvt_194df3105ad7148dcf2b98a91b5e727a=1551059328; __c=1551059340; __l=r=https%3A%2F%2Fwww.zhipin.com%2F%3Fsid%3Dsem_pz_bdpc_dasou_title&l=%2Fwww.zhipin.com%2Fjob_detail%2F%3Fquery%3D%25E6%25B5%258B%25E8%25AF%2595%25E5%25BC%2580%25E5%258F%2591%26scity%3D101250100%26industry%3D%26position%3D&g=%2Fwww.zhipin.com%2F%3Fsid%3Dsem_pz_bdpc_dasou_ti; lastCity=100010000; __a=94404191.1551059326.1551059326.1551059340.10.2.9.10; Hm_lpvt_194df3105ad7148dcf2b98a91b5e727a=1551066428; toUrl=https%3A%2F%2Fwww.zhipin.com%2Fjob_detail%2F%3Fquery%3D%25E6%25B5%258B%25E8%25AF%2595%25E5%25BC%2580%25E5%258F%2591%26scity%3D100010000%26industry%3D%26position%3D'
        }

    def run(self):
        '''
        启动爬虫
        :return:
        '''
        # 发送请求
        self.send_request()
        # 解析数据
        # 存储数据

    def send_request(self):
        '''
        遍历url地址，发送请求
        :return: 页面内容
        '''
        for url in self.urls:
            #发送请求,获取响应内容
            time.sleep(1)
            response = requests.get(url=url,headers=self.headers)
            #获取页面
            html_str =response.text
            #把页面转换成Element类型
            html = etree.HTML(html_str)
            self.get_data(html)

    def get_data(self,html):
        '''
        提取页面数据
        :param html: Element类型页面内容
        :return:
        '''
        # 提取数据的思路：
        # 首先定位到包含所有职位信息的标签，在提取里面所有的岗位标签,得到一个包含所有职位的列表
        job_list = html.xpath('//div[@class="job-list"]/ul/li')

        # 遍历列表，获取每一个岗位标签，提取该岗位的具体信息
        for job in job_list:
            # 提取岗位名称
            job_dict = {}
            job_dict['job_name'] = job.xpath('.//div[@class="job-title"]/text()')[0]
            # 提取岗位薪资
            job_dict['job_price'] = job.xpath('.//span[@class="red"]/text()')[0]
            # 提取职位相关信息
            job_dict['job_info'] = ' '.join(job.xpath('.//div[@class="info-primary"]/p/text()'))
            # 提取公司名称
            job_dict['company_name'] = job.xpath('.//div[@class="company-text"]/h3/a/text()')[0]
            # 提取公司相关信息
            job_dict['company_info'] = ''.join(job.xpath('.//div[@class="company-text"]/p/text()'))
            # 提取发布时间
            job_dict['job_time'] = ''.join(job.xpath('.//div[@class="info-publis"]/p/text()'))
            print(job_dict['job_time'],job_dict['job_name'])
            #储存数据
            self.write_data(job_dict)

    def write_data(self,data):
        '''
        存储数据
        :param data: 字典类型，每个职位的数据
        :return:
        '''
        with open('job2.csv', 'a', encoding='GBK') as f:
            headers=list(data.keys())
            csv_write = csv.DictWriter(f,fieldnames=headers)
            csv_write.writerow(data)



if __name__ == '__main__':
    spider = BossSpider()
    spider.run()

